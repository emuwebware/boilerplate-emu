<?php

global $emuTheme;

?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
  <!-- end CSS-->
  <?php wp_head(); ?>
</head>

<body>

  <div id="container">

    <header>

      <?php dynamic_sidebar( 'header' ); ?>

      <nav>
        <?php

        $aOptions = array(
          'theme_location'  => 'main',
          'container'       => false,
          'container_class' => '',
          'container_id'    => '',
          'menu_class'      => '',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => 'wp_page_menu',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 0);

        wp_nav_menu( $aOptions );

        ?>
      </nav>

    </header>

    <div id="main" role="main">

      <?php while ( have_posts() ) : the_post(); ?>

        <h1><?php the_title() ?></h1>

        <?php the_content() ?>

      <?php endwhile; ?>

    </div>

    <footer>

    </footer>

  </div> <!--! end of #container -->


  <!-- scripts concatenated and minified via ant build script-->
  <script defer src="<?php echo $emuTheme->sThemeURL?>/js/plugins.js"></script>
  <script defer src="<?php echo $emuTheme->sThemeURL?>/js/script.js"></script>
  <!-- end scripts-->

  <?php wp_footer(); ?>

</body>
</html>

<?php

class emuTheme extends emuApp
{
    function setAppConfig()
    {
        $this->emuAppID = 'emuTheme';
        $this->menuName = 'Emu Theme';
        $this->dbPrefix = 'emu_theme_';
    }

    function init()
    {
        $this->loadManager( 'theme', 'emuM_Theme', 'manage.theme.class.php' );
        $this->checkInstall();
    }
}


?>
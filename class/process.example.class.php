<?php

class emuP_Processor extends emuProcessor
{
	public $requiredFields = array('field_one', 'field_two');

	public function process()
	{
		switch( $this->button )
		{
			case 'Submit':

				$this->checkRequiredFields();

				if( !$this->hasRequiredFields )
				{
					$this->emuApp->addMessage( 'page', 'Not all required fields were provided', 'error' );
					$this->error = true;
					return;
				}

				break;
		}

		if( !$this->error )
		{
		 	header( 'Location: '.$sLocation );
		 	exit();
		}
	}
}

?>